import sqlite3
from sqlite3.dbapi2 import connect

DATABASE = 'nfties.db'

class Database():
    # конструктор бд (подключение)
    def __init__(self):
        self.conn = sqlite3.connect(DATABASE, check_same_thread=False)
        self.cur = self.conn.cursor()

    # вспомогательная функция для запроса
    def _getExecute(self, str):
        self.cur.execute(str)
        query = self.cur.fetchall()
        return query

    def checkmessage(self, name, day, notify):
        self.cur.execute(f'SELECT * FROM messages WHERE name = "{name}"')
        res = self.cur.fetchone()
        print(name, day, notify)
        print(res)
        if(res and res[1] == str(name) and res[2] == str(day) and res[3] == str(notify)):
            return True
        return False

    def addrow(self, message, day, notify):
        bday = (message, day, notify)
        self.cur.execute("INSERT INTO messages(name, day, notify) VALUES(?, ?, ?);", bday)
        self.conn.commit()