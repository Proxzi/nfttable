# Подключаем библиотеки
import httplib2 
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials	
from datetime import datetime as dt
from datetime import timedelta as td

spreadsheetId = ''

class Service():
    def __init__(self):
        self.service = self.auth()

    def auth(self):
        CREDENTIALS_FILE = ''  # Имя файла с закрытым ключом, вы должны подставить свое

        # Читаем ключи из файла
        credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive'])

        httpAuth = credentials.authorize(httplib2.Http()) # Авторизуемся в системе
        service = apiclient.discovery.build('sheets', 'v4', http = httpAuth) # Выбираем работу с таблицами и 4 версию API 
        print('https://docs.google.com/spreadsheets/d/' + spreadsheetId)
        return service

    def delete_nondelta(self, rows):
        i = 0
        m = len(rows)
        while i <m:
            if type(rows[i][0]) == str:
                del rows[i]
                m-=1
            else:
                i+=1
        return rows


    def get_rows(self):
        ranges = ["Сейлы!A:G"] 
        results = self.service.spreadsheets().values().batchGet(spreadsheetId = spreadsheetId, 
                                            ranges = ranges, 
                                            valueRenderOption = 'FORMATTED_VALUE',  
                                            dateTimeRenderOption = 'FORMATTED_STRING').execute() 
        rows = results['valueRanges'][0]['values']

        del rows[0:2]
        for i in range(len(rows)):
            if i < 2 or len(rows[i]) < 6:
                continue
            if rows[i][0] != 'TBA' and rows[i][0] != '':
                rows[i][0]+=".2021"
                date = None
                #print(rows[i])
                if rows[i][1] != 'TBA' and rows[i][1] != '':
                    tmp = rows[i][0] + f' {rows[i][1]}'
                    date = dt.strptime(tmp, '%d.%m.%Y %H:%M')
                    notifyHours = date - td(hours=1)
                    notifyFifteen = date - td(minutes=15)
                    timedata = {'15': notifyHours,'hour': notifyFifteen}
                    rows[i][0] = timedata
            
        return self.delete_nondelta(rows)