from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.utils import executor
from aiogram.types import message
from readTable import Service
import time
import asyncio
import aioschedule
from datetime import datetime as dt
from datetime import timedelta as td
from handler import Database

db = None

TOKEN = ''

CHAT_ID = 
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


googleService = None

rows = None

@dp.message_handler(commands=['start'])
async def chatid(message: types.Message):
    await message.answer(text=f'chat_id = {message.chat.id}')

async def get_rows():
    rows = googleService.get_rows()
    return rows

async def get_table():
    aioschedule.every(5).seconds.do(get_rows)
    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(1)

def convertRow(row):
    result = f'Время минта: сегодня в {row[1]}\nНазвание: {row[2]}\nЦена: {row[4]} {row[5]}\n{row[6]}\n{row[3]}'
    return result
    
async def notify(now,row,time):
    if now.date() == row[0][time].date():
        if(now.time().hour == row[0][time].time().hour and now.time().minute == row[0][time].time().minute):
            if not db.checkmessage(row[2],row[0][time].date(), time):
                print(db.checkmessage(row[2],row[0][time].date(), time))
                db.addrow(row[2], row[0][time].date(), time)
                text = convertRow(row)
                await bot.send_message(CHAT_ID, text=text)
                print('nice')

async def scheduler():
    while True:
        now = dt.now()
        deltahour = now - td(hours=1)
        delta15min = now - td(minutes=15)
        for row in rows:
            await notify(delta15min, row,'15')
            await notify(deltahour, row,'hour')
        await asyncio.sleep(1)

async def on_startup(_):
    global db, googleService, rows
    db = Database()
    googleService = Service()
    rows = googleService.get_rows()
    asyncio.create_task(get_table())
    asyncio.create_task(scheduler())


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=False, on_startup=on_startup)